import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Switch, Route } from 'react-router-dom';

import './src/style.reset.scss';

import Home from './src/components/home';
import fourOfour from './src/components/fourOfour';

const App = ({ store, history }) => (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/fakeHome" component={Home} />
                <Route component={fourOfour} />
            </Switch>
        </ConnectedRouter>
    </Provider>
);

export default App;
