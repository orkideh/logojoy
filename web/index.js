import 'babel-polyfill';
import 'raf/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import { configureInterceptors } from './app/helpers/fetch';

import configureStore from './app/store/configureStore';

import App from './App';

const appBootstrap = () => {
    const rootElement = document.getElementById('root');
    if (rootElement === null) {
        throw new Error('no root element');
    }

    const history = createHistory();
    const store = configureStore(history);
    configureInterceptors(store);
    // const store = configureStore();
    ReactDOM.render(
        <App store={store} history={history}/>,
        rootElement
    );
};

appBootstrap();
