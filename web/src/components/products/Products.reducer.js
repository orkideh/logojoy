import {
    CLEAR_PRODUCTS,
    LOAD_PRODUCTS
} from "./Products.constans";

export const productReducer = (state = [], action) => {
    switch (action.type) {
        case LOAD_PRODUCTS:
            return [
                ...state,
                ...action.payload
            ];
        case CLEAR_PRODUCTS:
            return [];
        default: {
            return state;
        }
    }
};
