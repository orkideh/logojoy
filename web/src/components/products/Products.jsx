import React, { useEffect, useState, Fragment } from 'react';
import { connect } from 'react-redux';
import { Container, Row } from 'react-grid-system';
import InfiniteScroll from 'react-infinite-scroller';

import styles from './style.scss';

import ProductComponent from '../product';
import { loadProducts } from './Product.actions';
import { selectProductContext } from './Products.selectors';
import { selectLoadingContext } from '../Loading/Loading.selctors';
import SortComponent from '../sort';
import Loading from '../Loading';
import Ad from '../ad';
import NoProdcusts from '../noProducts';

const ProductsComponent = ({ products, loading, loadProducts }) => {

    useEffect(() => {
        (async function useEffectAsync() {
           const result = await loadProducts();
           /*
            Not using result just to make sure of code order execution
            checking if enough product loaded in initial load that activate the scrollbar else we load one more batch
             */
            if(window.innerWidth === document.documentElement.clientWidth) {
                await loadProducts();
            }
        })()
    }, [] );

    const renderProducts = (product, index) => {
        if (index > 0 && index % 20 === 0){
            return (
                <Fragment key={product.id}>
                    <Ad index={index}/>
                    <ProductComponent product={product}/>
                </Fragment>
            );
        }
        return (<ProductComponent key={product.id} product={product}/>);
    };

    return (
        <Container>
            <Row className={styles.container}>
                <SortComponent />
            </Row>
            <InfiniteScroll
                initialLoad={false}
                hasMore={ loading ? false : true }
                threshold={500}
                loadMore={ () => loadProducts(products.length) }
            >
                <Row className={styles.container}>
                    {products.map(renderProducts)}
                </Row>
            </InfiniteScroll>
            <Row className={styles.container}>
                {loading
                &&
                (
                    <Loading loading={loading}/>
                )
                }
                {!products.length && !loading
                &&
                (
                    <NoProdcusts />
                )
                }
            </Row>
        </Container>
    );
};

const mapDispatchToProps = {
    loadProducts,
};

const mapStateToProps = (state) => ({
    products: selectProductContext(state),
    loading: selectLoadingContext(state),
});


export default connect(mapStateToProps, mapDispatchToProps)(ProductsComponent)
