import { LOAD_PRODUCTS, CLEAR_PRODUCTS } from './Products.constans';
import { fetchProducts } from '../../../app/helpers/fetch';

const setProducts = (products = []) => ({
    type: LOAD_PRODUCTS,
    payload: products
});

export const loadProducts = (skip, limit) => async (dispatch) => {
    const products = await fetchProducts(skip, limit);
    dispatch(setProducts(products));
};

const setProductsEmpty = () => ({
    type: CLEAR_PRODUCTS
});

export const clearProducts = () => (dispatch) => (dispatch(setProductsEmpty()));
