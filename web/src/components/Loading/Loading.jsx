import React, { Fragment } from 'react';
import styles from './styles.scss';

export default ({ loading }) => (
    <Fragment>
        {loading &&
            <p className={styles.loading}>
                Loading...
            </p>
        }
    </Fragment>
);
