import React from 'react';
import renderer from 'react-test-renderer';

import Loading from './Loading';
import { shallow } from '../../../../test/enzyme';

test('Loading component general rendering in state of Loading', () => {
    const component = renderer
        .create(
            <Loading loading={true} />,
        );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('Loading component general rendering in state of no Loading', () => {
    const component = renderer
        .create(
            <Loading loading={false} />,
        );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('Loading component text should match expected', () => {
    const wrapper = shallow(<Loading loading={true} />);
    expect(wrapper.text()).toBe('Loading...');
});

