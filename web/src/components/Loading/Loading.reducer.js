import {SET_START_LOADING, SET_STOP_LOADING} from "./Loading.constants";

export const LoadingReducer = (state = false, action) => {
    switch (action.type) {
        case SET_START_LOADING: {
            return true;
        }
        case SET_STOP_LOADING: {
            return false;
        }
        default: {
            return state;
        }
    }
};
