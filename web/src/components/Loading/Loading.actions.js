import { SET_START_LOADING, SET_STOP_LOADING } from './Loading.constants';

export const setStartLoading = () => ({
    type: SET_START_LOADING,
    payload: null
});

export const setStopLoading = () => ({
    type: SET_STOP_LOADING,
    payload: null
});
