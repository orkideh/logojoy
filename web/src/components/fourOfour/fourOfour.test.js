import React from 'react';
import renderer from 'react-test-renderer';

import FourOfour from './fourOfour';
import { shallow } from '../../../../test/enzyme';

test('FourOFour component general rendering', () => {
    const component = renderer
        .create(
            <FourOfour />,
        );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('FourOFour component text should match expected', () => {
    const wrapper = shallow(<FourOfour />);
    expect(wrapper.text()).toBe('Nothing found here Buddy');
});

