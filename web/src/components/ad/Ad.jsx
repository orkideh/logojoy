import React, { useEffect } from 'react';

import styles from './styles.scss';

export default ({ index }) => {
    return (
        <div className={styles.ad}>
            <img src={`/ad?r=${index}`} alt="Ad"/>
        </div>
    );
};

