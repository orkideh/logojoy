import React from 'react';
import { shallow } from '../../../../test/enzyme';

import renderer from 'react-test-renderer';
import Ad from './Ad';

test('Ad component general rendering and index params change', () => {
    let index = 1;
    const component = renderer
        .create(
            <Ad index={index} />,
        );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    tree.props.index = 2;
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

const hasImage = wrapper => wrapper.find('img').length === 1;
test('Ad component should have image element', () => {
    const index = 1;
    const wrapper = shallow(<Ad index={index} />);
    expect(hasImage(wrapper)).toBe(true);
});

