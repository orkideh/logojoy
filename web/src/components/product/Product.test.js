import React from 'react';
import renderer from 'react-test-renderer';

import Product from './Product';
import { shallow } from '../../../../test/enzyme';

test('Product component general rendering', () => {
    const product = {
        id: 1,
        size: 10,
        price: 566,
        face: '( ⚆ _ ⚆ )'
    };
    const component = renderer
        .create(
            <Product product={product}/>,
        );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('Product component should have p element with desired fontsize', () => {
    const product = {
        id: 1,
        size: 10,
        price: 566,
        face: '( ⚆ _ ⚆ )'
    };
    const wrapper = shallow(<Product product={product}/>);
    expect(wrapper.find('p').length).toBe(2);
    expect(wrapper.find('p').get(0).props.style.fontSize).toBe(10);
    expect(wrapper.find('p').get(1).props.children).toBe('$5.66');
});

