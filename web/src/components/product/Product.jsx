import React from 'react';
import { Col } from 'react-grid-system';

import styles from './style.scss';

import { cent2dollar } from '../../../app/helpers/unitConvertor';

export default ({ product: { id, size, price, face } }) => {
    return (
        <Col
            sm={6}
            md={4}
            xl={3}
            className={styles.product}
        >
            <p style={{ fontSize: size }} className={styles.face}>
                {face}
            </p>
            <p className={styles.price}>
                { cent2dollar(price) }
            </p>
        </Col>
    );
};

