import React from 'react';
import renderer from 'react-test-renderer';

import NoProducts from './NoProducts';
import { shallow } from '../../../../test/enzyme';

test('NoProduct component general rendering', () => {
    const component = renderer
        .create(
            <NoProducts />,
        );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('NoProduct component text should match expected', () => {
    const wrapper = shallow(<NoProducts />);
    expect(wrapper.text()).toBe('~ end of catalogue ~');
});

