import React from 'react';
import styles from "./styles.scss";

export default () => {
    return (
        <p className={styles.noProducts}>~ end of catalogue ~</p>
    );
}
