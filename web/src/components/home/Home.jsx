import React, { useState, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { Container } from 'react-grid-system';

import styles from './styles.scss';

import ProductsComponent from '../products';

export default function Home() {
    const [name, setName] = useState('name');

    function handleNameChange(e) {
        setName(e.currentTarget.value);
    }

    return (
        <Fragment>
            <ProductsComponent />

        </Fragment>
    )
}
