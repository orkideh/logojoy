import { SET_SORT } from './Sort.constants';

const setSort = (sort) => ({
    type: SET_SORT,
    payload: sort,
});

export const updateSort = (sort = 'Default') => (dispatch) => {
    dispatch(setSort(sort));
};
