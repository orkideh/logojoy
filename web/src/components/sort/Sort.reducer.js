import { SET_SORT } from './Sort.constants';

export default (state = 'Default', action) => {
    switch (action.type) {
        case SET_SORT: {
            return action.payload;
        }
        default: {
            return state;
        }
    }
};
