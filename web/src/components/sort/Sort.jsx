import React from 'react';
import { connect } from 'react-redux';

import { loadProducts, clearProducts } from '../products/Product.actions';

import { updateSort } from './Sort.actions';
import { options } from './Sort.constants';

const SortComponent = ({ updateSort, loadProducts, clearProducts }) => {
    return (
        <div>
            <label>
                Sort
            </label>
            <select
                onChange={ event => {
                    updateSort(event.currentTarget.value);
                    clearProducts();
                    loadProducts();
                }}
            >
                <option>Default</option>
                {options.map(sort => (<option key={sort}>{sort}</option>))}
            </select>
        </div>
    );
};

const mapDispatchToProps = {
    updateSort,
    loadProducts,
    clearProducts,
};

export default connect(null, mapDispatchToProps)(SortComponent);

