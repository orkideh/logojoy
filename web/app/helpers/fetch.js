import axios from 'axios';
import {
    SET_START_LOADING,
    SET_STOP_LOADING
} from '../../src/components/Loading/Loading.constants';
import { options } from '../../src/components/sort/Sort.constants';
import { selectSortContext } from '../../src/components/sort/Sort.selector';

export const fetchProducts = async (skip = 0, limit = 20) => {
    const products = await axios
        .get(`/api/products?limit=${limit}&skip=${skip}`);
    return products
        .data;
};

const setStartLoading = () => ({ type: SET_START_LOADING });

const setStopLoading = () => ({ type: SET_STOP_LOADING });

export const configureInterceptors = store => {
    axios
        .interceptors
        .request
        .use(function (config) {
            const sort = selectSortContext(store.getState());
            if(typeof sort !== "undefined" && options.includes(sort)) {
                config.url += '&' + new URLSearchParams({ sort });
            }
            store.dispatch(setStartLoading());
            return config;
    }, function (error) {
            store.dispatch(setStartLoading());
            return Promise.reject(error);
    });
    axios
        .interceptors
        .response
        .use(function (response) {
            store.dispatch(setStopLoading());
            return response;
    }, function (error) {
            store.dispatch(setStopLoading());
            console.log('keke response just arrived');
            return Promise.reject(error);
    });
};
