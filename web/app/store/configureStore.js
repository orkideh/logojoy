import  { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

const isProd = process.env.NODE_ENV !== 'production';

import createRootReducer from '../reducers/rootReducer';

export default function configureStore(history) {
    const middlewares = [thunk, routerMiddleware(history)];
    const middlewareEnhancer = applyMiddleware(...middlewares);

    const store = createStore(
        createRootReducer(history),
        { products: [] },
        isProd ? middlewareEnhancer : composeWithDevTools(middlewareEnhancer),
    );

    if (isProd) {
        if (module.hot) {
            module.hot.accept('../reducers/rootReducer', () => {
                store.replaceReducer(createRootReducer(history));
            });
        }
    }

    return store;
}
