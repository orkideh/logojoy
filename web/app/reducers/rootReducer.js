import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { productReducer } from '../../src/components/products/Products.reducer';
import { LoadingReducer } from '../../src/components/Loading/Loading.reducer';
import SortReducer from '../../src/components/sort/Sort.reducer';

const reducers = {
    products: productReducer,
    loading: LoadingReducer,
    sort: SortReducer,
};

const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    ...reducers,
});

export default createRootReducer;
