const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const flexFixes = require('postcss-flexbugs-fixes');

const postCSS = {
    loader: 'postcss-loader',
    options: {
        ident: 'postcss',
        plugins: () => [
            flexFixes,
            autoprefixer({ browsers: ['last 2 versions', 'ie >= 11'] }),
        ],
    },
};

module.exports = {
    mode: 'development',
    entry: './web/index.js',
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                exclude: /(node_modules)/,
                resolve: {
                    extensions: [".js", ".jsx"]
                },
                loader: 'babel-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    'style-loader',
                    {
                        loader: "css-loader",
                        options: {
                            modules: true
                        }
                    },
                    postCSS,
                    'sass-loader',
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader'
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Output Management',
            template: './web/public/index.html'
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
        contentBase: 'dist',
        historyApiFallback: true,
        headers: {
            'X-Who': 'Ali'
        },
        overlay: true,
        proxy: {
            '/api': 'http://localhost:8000/api',
            '/ad': 'http://localhost:8000/ad'
        }
    }
};
