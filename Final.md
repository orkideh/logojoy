How to run 
===

* please run `yarn install --prod=false`
* please run the back-end on the dev mode by `yarn dev:server`.
* please run the app by `yarn dev`.
* then you can open browser [`http://localhost:8080`](http://localhost:8080) and check the application.

How to build for production
===

* please execute `yarn build`;
* the result will be in `dist` folder.

How to run tests
===

* please execute `yarn test`.


Known issues and notes
===

* Tests are not the best with full coverage just a few to show what is possible to cover.
* No E2E test are included because almost there is not user interaction considered.
* For the ` "~ end of catalogue ~"` message  I could not found any case that we are end of catalogue. so it`s shown just in case there is not product to show and application is not requesting any products.
* pre-loading triggered when user has 500px scroll till end of the page.
 
